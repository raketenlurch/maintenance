#!/bin/bash
# created by peleke.de (Ghost Guide), customized by logazer

WIKIDIR=~/wiki
PACKAGE_VERSION_OLD=$(sed -nE 's/^\*"version": "(.*?)",$/\1/p' $WIKIDIR/package.json)
CURRENT_WIKI=$(curl -s https://api.github.com/repos/Requarks/wiki/releases/latest | grep tag_name | head -n 1 | cut -d '"' -f 4)
CURRENT_WIKI_DOWNLOAD="https://github.com/Requarks/wiki/releases/download/$CURRENT_WIKI/wiki-js.tar.gz"

if [[ $1 == "--rollback" ]]
then
  if [[ -d ~/tmp/wiki.bak ]]
  then
    PACKAGE_VERSION_BACKUP=$(sed -nE 's/^\s*"version": "(.*?)",$/\1/p' ~/tmp/wiki.bak/package.json)
    echo "Stopping Wiki.js..."
    supervisorctl stop wiki
    echo "Rolling back to $PACKAGE_VERSION_BACKUP..."
    rm -rf $WIKIDIR/* && cp -r ~/tmp/wiki.bak/* $WIKIDIR
    echo "Rollback complete..."
    echo "Starting Wiki.js. This may take a few seconds..."
    supervisorctl start wiki
    exit 0
  else
    echo "No Backup found. :("
    exit 1
  fi
fi

if [[ $CURRENT_WIKI != $PACKAGE_VERSION_OLD ]]
then
  read -r -p "Do you want to update Wiki.js $PACKAGE_VERSION_OLD to version $CURRENT_WIKI? [Y/n] " response
  if [[ $response =~ ^([yY]|"")$ ]]
  then
    echo "Updating Wiki.js ..."
    echo "Downloading version $CURRENT_WIKI ..."
    (cd ~/tmp/ && curl --progress-bar -LO $CURRENT_WIKI_DOWNLOAD)
    echo "Stopping Wiki.js ..."
    supervisorctl stop wiki
    echo "Removing version $PACKAGE_VERSION_OLD ..."
    cp $WIKIDIR/config.yml ~/tmp/config.yml.bak
    rm -rf ~/tmp/wiki.bak
    cp -r $WIKIDIR ~/tmp/wiki.bak
    rm -rf $WIKIDIR/*
    echo "Extracting version $CURRENT_WIKI ..."
    tar xzf ~/tmp/wiki-js.tar.gz -C $WIKIDIR && rm ~/tmp/wiki-js.tar.gz
    cp ~/tmp/config.yml.bak $WIKIDIR/config.yml && rm ~/tmp/config.yml.bak

    PACKAGE_VERSION=$(sed -nE 's/^\s*"version": "(.*?)",$/\1/p' $WIKIDIR/package.json)
    echo "Wiki.js $PACKAGE_VERSION_OLD has been updated to version $PACKAGE_VERSION"
    echo "Starting Wiki.js. This may take a few seconds ..."
    supervisorctl start wiki
    supervisorctl status
    echo "If something seems wrong, please check the logs: 'supervisorctl tail wiki'"
    echo "To rollback the update to the last version, use the option '--rollback'"
  fi
else
  echo "Wiki.js $PACKAGE_VERSION_OLD is already up-to-date, no update needed."
fi
